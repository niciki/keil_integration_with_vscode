# Keil integration into VSCode and STM32CubeIDE

Bunch of scripts for Python 3.x and Batch that integrate Keil's project and toolchain into VSCode and STM32CubeIDE.  
Also contains base folder's structure for Keil's project.   
It compiles project and parses errors/warnings/infos into VSCode and STM32CubeIDE.

For more information, visit following blog entries:  
[Keil integration into VSCode](https://www.edaboard.com/blog/keil-vscode-keil-integration-into-vscode.2103/)   
[Keil integration into STM32CubeIDE](https://www.edaboard.com/blog/keil-stm32cubeide-keil-integration-into-stm32cubeide.2116/)   

# Usage
1. Install Python 3 and add its location to PATH;
2. Satisfy Python's requirements needed by this toolset (make sure to use Python 3.x instead of Python 2.x), by:  
`python ./Scripts/install.py`
3. Copy .vscode and scripts folders into the root folder of your Keil's project (with Src, Inc and perhaps MDK-ARM subfolder);
4. Open root folder of your Keil's project by __Open with Code__ context menu; 

In VSCode:

5. Navigate to Keil's project file (.uvproj or .uvprojx) and open it;
6. Run __Run Build Task...__ and choose from the list __Import settings from Keil's v4 or v5 project__ - that will import includes, defines and output paths for all project's targets from opened project file;
7. Choose Keil's project target by __C/C++: Select a Configuration...__ command in VSCode;
8. Run again __Run Build Task...__ and choose desired feature, e.g. clean/build/rebuild/program.

# Available features  
### ![](overview.png)  
### Init
* __Import settings from Keil's v4 or v5 project__ - imports includes, defines and output paths for all project's targets
### Building
* __Build by Keil__ - builds Keil's project for a chosen target
* __ALL build by Keil__ - builds Keil's project for all targets (make sure that all targets have different output paths in Keil's project)
* __[DBG] ALL build by Keil__ - builds Keil's project for all targets that name starts with "[DBG]..."
* __[REL] ALL build by Keil__ - builds Keil's project for all targets that name starts with "[REL]..."
### Rebuilding
* __Rebuild by Keil__ - rebuilds Keil's project for a chosen target
* __[DBG] ALL rebuild by Keil__ - rebuilds Keil's project for all targets that name starts with "[DBG]..."
* __[REL] ALL rebuild by Keil__ - rebuilds Keil's project for all targets that name starts with "[REL]..."
* __ALL rebuild by Keil__ - rebuilds Keil's project for all targets (make sure that all targets have different output paths in Keil's project)
### Cleaning
* __ALL clean by Keil__ - cleans Keil's output files for all targets
### Programming
* __Program Flash by Keil__ - downloads the application to Flash using Keil's settings of a chosen target and available programming device
### Opening in Keil
* __Open project in Keil__ - opens Keil's project in native IDE

# Customisable summary in Python
### ![](customisable_summary.png)  