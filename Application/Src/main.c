/* Includes -----------------------------------------------------------------*/
#include "main.h"
#include "stdint.h"


/* Defines ------------------------------------------------------------------*/
const uint8_t kMaxValue = 0xFF;


/* Global variables ---------------------------------------------------------*/


/* Static variables ---------------------------------------------------------*/


/* External variables -------------------------------------------------------*/


/* Private functions --------------------------------------------------------*/


/* Public functions ---------------------------------------------------------*/
int main(void) {
  uint8_t temp = kMaxValue;
  while(1) {
    if (temp == 0) {
      uint8_t temp2 = kMaxValue;  // make error here, e.g. by renaming kMaxValue to kMaxValue2
    } else {
      temp--;
    }
  }
}

